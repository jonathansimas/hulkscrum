(function( $ ){

$.fn.monsterValidator = function( options ) {  

        var settings = $.extend( {
            'para'         : 'top',
            'background-color' : 'blue'
        }, options);

        var methods = {
            init : function() { 
                $(this).bind('submit', methods.validateTextRequired);               
            },
            validateTextRequired : function(e) 
            {               
                var requiredTextInputs = $('input.required[type=text]', this);
                var validated = true;
                requiredTextInputs.each(function(ev)
                {
                    if($(this).val() == '' && validated == true)
                    {
                        alert("Opa! O campo "+$(this).attr('name')+ ' é obrigatório');                      
                        validated = false;
                        return;
                    }
                    if($(this).hasClass("numeric") && validated)
                    {
                        validated = methods.validateNumeric($(this));
                    }
                });
                return validated;
            },
            validateNumeric : function(obj) 
            {               
                var numbers = /^[0-9]+$/;  
                if(!obj.val().match(numbers))  
                {
                    alert("Opa! O campo "+obj.attr('name')+ ' aceita somente caracteres numericos');       
                    return false;
                }
                return true;
            },
            validateSelectRequired : function(e) 
            {               
                var requiredSelectInputs = $('option[selected]', 'select.required');
                
                
                var validated = true;
                requiredSelectInputs.each(function(ev)
                {
                    if($(this).val() == '' && validated == true)
                    {
                        alert("Opa! O campo "+$(this).attr('name')+ ' é obrigatório');                      
                        validated = false;
                        return;
                    }
                });
                return validated;
            },
            hide : function( ) { 
            // GOOD
            },
            update : function( content ) { 
            // !!! 
            }
        };
        
        return this.each(function(method ) 
        { 
            if ( methods[method] ) 
            {
                return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
            } 
            else if ( typeof method === 'object' || ! method ) 
            {
                return methods.init.apply( this, arguments );
            } 
            else 
            {
                $.error( 'Metodo ' +  method + ' nao existe' );
            }    
  
        });
  
    };
})( jQuery );

