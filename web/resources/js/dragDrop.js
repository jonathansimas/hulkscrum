
function ok(){
  
    if($("[name='formulario:ajaxResponse']").val() == 'ok')
    {
        var item = $(".pinTask[idRef='"+$("[name='formulario:idTarefa']").val()+"']");
                
        if(item.parent().attr('id') == 'quadroPendente')
        {
            item.removeClass("andamentoTask");
            item.removeClass("finalizadoTask");
            item.addClass("pendenteTask")
        }
        if(item.parent().attr('id') == 'quadroAndamento')
        {
            item.removeClass("andamentoTask");
            item.removeClass("pendenteTask");
            item.addClass("andamentoTask");
        }
        if(item.parent().attr('id') == 'quadroFinalizado')
        {
            item.removeClass("andamentoTask");
            item.removeClass("pendenteTask");
            item.addClass("finalizadoTask");
        }            
    }
   
}

$(function() {
    
    var oldList, newList, item;
    
    $('.sortable').sortable({
        start: function(event, ui) {
            item = ui.item;
            newList = oldList = ui.item.parent();
        },
        stop: function(event, ui) {     
            //console.log('NewList:'+ui.item.parent().attr('idRef'))
            //console.log('oldList:'+oldList.attr('idRef'))
            if(ui.item.parent().attr('idRef') !=  oldList.attr('idRef'))
            {
                $("[name='formulario:coluna']").val(ui.item.parent().attr('idRef'));
                $("[name='formulario:idTarefa']").val(ui.item.attr('idRef'));            
                updateColumns();           
            }            
        },
        change: function(event, ui) {  
            if(ui.sender) newList = ui.placeholder.parent().parent();
        },
        connectWith: ".sortable"
    }).disableSelection();
});