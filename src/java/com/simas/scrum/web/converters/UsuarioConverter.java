/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.web.converters;


import com.simas.scrum.dao.UsuarioDAO;
import com.simas.scrum.entity.Usuario;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author c3712548
 */
@FacesConverter(value="usuarioConverter", forClass=Usuario.class)  
public class UsuarioConverter implements Converter {  
  
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) { 
        if(value.equals(""))
        {
            return null;
        }
        return (Usuario)UsuarioDAO.getInstance().getById(Integer.valueOf(value));
    }  
  
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {  
        if(value == null || value.equals("")) {
            return "";
        }
        return String.valueOf(((Usuario) value).getIdUsuario());
    }  
  
} 
