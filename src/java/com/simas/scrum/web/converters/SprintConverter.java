/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.web.converters;


import com.simas.scrum.dao.SprintDAO;
import com.simas.scrum.entity.Sprint;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


@FacesConverter(value="sprintConverter", forClass=Sprint.class)  
public class SprintConverter implements Converter {  
  
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) { 
        if(value.equals(""))
        {
            return null;
        }
        return (Sprint)SprintDAO.getInstance().getById(Integer.valueOf(value));
    }  
  @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {  
        if(value == null || value.equals(""))
            return "";
        return String.valueOf(((Sprint) value).getIdSprint());
    }  
  
} 
