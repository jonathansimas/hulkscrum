package com.simas.scrum.web.converters;





import com.simas.scrum.dao.EquipeDAO;
import com.simas.scrum.entity.Equipe;
import java.util.Date;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value="dateConverter", forClass=Date.class)  
public class DateConverter  implements Converter {  
    
        @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) { 
        if(value.equals(""))
        {
            return null;
        }
        return (Equipe)EquipeDAO.getInstance().getById(Integer.valueOf(value));
    }  
  
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {  
        if(value == null || value.equals(""))
            return "";
        return String.valueOf(((Equipe) value).getIdEquipe());
    }  
  
    
}
