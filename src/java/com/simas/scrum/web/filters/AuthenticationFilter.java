package com.simas.scrum.web.filters;

import com.simas.scrum.named.LoginNamed;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns={"/usuario/*", "/equipe/*","/sprint/*","/projeto/*","/tarefa/*","/quadro/*", "/index.xhtml"})
public class AuthenticationFilter implements Filter {
     
    @Inject
    private LoginNamed loginNamed;

	  @Override
	    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

	        HttpServletRequest req = (HttpServletRequest) request;
	        HttpSession session = req.getSession();
            HttpServletResponse res = (HttpServletResponse) response;
            LoginNamed login = loginNamed;
            if(req.getRequestURI().endsWith(".css.xhtml") 
            		|| req.getRequestURI().endsWith(".js.xhtml")
            		|| req.getRequestURI().endsWith(".png.xhtml")
            		|| req.getRequestURI().endsWith(".jpg.xhtml")
            		|| req.getRequestURI().endsWith(".gif.xhtml")
            		)
            {
            	chain.doFilter(request, response);
            }            	
            else if (( login != null && login.getUsuario() != null &&  login.getUsuario().getIdUsuario() != null)
	        		|| req.getRequestURI().endsWith("Login.xhtml")) {
		            chain.doFilter(request, response);
	        	}
	        	else
	        	{
		            res.sendRedirect(req.getContextPath()+"/login/Login.xhtml");
		            return;
		        }
	    }

	    @Override
	    public void init(FilterConfig filterConfig) throws ServletException {

	    }

	    @Override
	    public void destroy() {
	    }
	}