/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.dao;

import com.simas.scrum.core.dao.GenericDAO;
import com.simas.scrum.entity.Usuario;

/**
 *
 * @author c3712548
 */
public class UsuarioDAO extends GenericDAO<Usuario> {
    
	private static UsuarioDAO dao = null;
	
	public static UsuarioDAO getInstance()
	{
		if(dao == null )
		{
			dao = new UsuarioDAO();
		}
		dao.setType(Usuario.class);
		
		return dao;		
	}
}
