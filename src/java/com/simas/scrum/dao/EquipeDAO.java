/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.dao;

import com.simas.scrum.core.dao.GenericDAO;
import com.simas.scrum.entity.Equipe;

/**
 *
 * @author c3712548
 */
public class EquipeDAO extends GenericDAO<Equipe> {
    
	private static EquipeDAO dao = null;
	
        
	public static EquipeDAO getInstance()
	{
		if(dao == null )
		{
			dao = new EquipeDAO();
		}
		dao.setType(Equipe.class);
		dao.clear();
		return dao;		
	}
}
