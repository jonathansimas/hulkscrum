/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.dao;

import com.simas.scrum.core.dao.GenericDAO;
import com.simas.scrum.entity.Projeto;

/**
 *
 * @author c3712548
 */
public class ProjetoDAO extends GenericDAO<Projeto> {
    
	private static ProjetoDAO dao = null;
	
	public static ProjetoDAO getInstance()
	{
		if(dao == null )
		{
			dao = new ProjetoDAO();
		}
		dao.setType(Projeto.class);
		dao.clear();
		return dao;		
	}
}
