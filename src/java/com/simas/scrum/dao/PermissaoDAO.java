/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.dao;

import com.simas.scrum.core.dao.GenericDAO;
import com.simas.scrum.entity.Permissao;

/**
 *
 * @author c3712548
 */
public class PermissaoDAO extends GenericDAO<Permissao> {
    
	private static PermissaoDAO dao = null;
	
	public static PermissaoDAO getInstance()
	{
		if(dao == null )
		{
			dao = new PermissaoDAO();
		}
		dao.setType(Permissao.class);
		dao.clear();
		return dao;		
	}
}
