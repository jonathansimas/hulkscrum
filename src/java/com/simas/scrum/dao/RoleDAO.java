/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.dao;

import com.simas.scrum.core.dao.GenericDAO;
import com.simas.scrum.entity.Role;

/**
 *
 * @author c3712548
 */
public class RoleDAO extends GenericDAO<Role> {
    
	private static RoleDAO dao = null;
	
	public static RoleDAO getInstance()
	{
		if(dao == null )
		{
			dao = new RoleDAO();
		}
		dao.setType(Role.class);
		dao.clear();
		return dao;		
	}
}
