/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.dao;

import com.simas.scrum.core.dao.GenericDAO;
import com.simas.scrum.entity.Sprint;

/**
 *
 * @author c3712548
 */
public class SprintDAO extends GenericDAO<Sprint> {
    
	private static SprintDAO dao = null;
	
	public static SprintDAO getInstance()
	{
		if(dao == null )
		{
			dao = new SprintDAO();
		}
		dao.setType(Sprint.class);
		dao.clear();
		return dao;		
	}
}
