/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.dao;

import com.simas.scrum.core.dao.GenericDAO;
import com.simas.scrum.entity.Tarefa;

/**
 *
 * @author c3712548
 */
public class TarefaDAO extends GenericDAO<Tarefa> {
    
	private static TarefaDAO dao = null;
	
	public static TarefaDAO getInstance()
	{
		if(dao == null )
		{
			dao = new TarefaDAO();
		}
		dao.setType(Tarefa.class);
		dao.clear();
		return dao;		
	}
}
