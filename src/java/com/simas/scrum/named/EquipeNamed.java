package com.simas.scrum.named;

import com.simas.scrum.business.util.JsfUtil;
import com.simas.scrum.business.util.MessagesController;
import com.simas.scrum.dao.EquipeDAO;
import com.simas.scrum.entity.Equipe;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author c3712548
 */
@Named("equipe")
@RequestScoped
public class EquipeNamed {

//    @PersistenceContext(unitName="scrumPU2")
//    EntityManager entidade;
    @Inject
    private Equipe equipe;
    @Inject
    private EquipeDAO equipeDAO;
    private List<Equipe> equipes;
    private String termoBusca;
    private LazyDataModel<Equipe> lazyModel;
    private int totalRows;
    private int pageSize;

    public LazyDataModel<Equipe> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Equipe> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public String getTermoBusca() {
        return termoBusca;
    }

    public void setTermoBusca(String termoBusca) {
        this.termoBusca = termoBusca;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public EquipeDAO getEquipeDAO() {
        return equipeDAO;
    }

    public void setEquipeDAO(EquipeDAO equipeDAO) {
        this.equipeDAO = equipeDAO;
    }

    public List<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(List<Equipe> equipes) {
        this.equipes = equipes;
    }


    public void save() {      
        try {
            equipeDAO.save(equipe);
            MessagesController.addInfo(null, "A equipe foi salva com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }
    }
    public void update() {
      // Equipe test = entidade.find(Equipe.class, equipe.getIdEquipe());
        try {
            equipeDAO.update(equipe);
            MessagesController.addInfo(null, "A equipe foi salva com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }
    }

    public void delete() {
        equipeDAO.delete(equipe);
    }

    public void delete(Equipe equipe) {
        equipeDAO.delete(equipe);
    }

    public List<Equipe> listarEquipes() {
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            Equipe equipeExemplo = new Equipe();
            equipeExemplo.setNome(termoBusca);

            return equipeDAO.getBySimilarity(equipeExemplo);
        }
        return equipeDAO.getAll();
    }

    public String carregarEdicao(Equipe equipe) throws IOException {
        this.equipe = equipe;
        return "Edit.xhtml";
    }

    public String carregarNovaEquipe() {
        this.equipe = new Equipe();
        return "Create.xhtml";
    }

    public String carregarListarEquipes() {
        this.listarEquipes();
        return "List.xhtml";
    }

    public String limparListarEquipes() {
        this.setTermoBusca("");
        this.listarEquipes();
        return "List.xhtml";
    }

    @PostConstruct
    public void init() {
 HttpSession session;
        equipeDAO = EquipeDAO.getInstance();
        if(FacesContext.getCurrentInstance() != null)
        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }

    public void updateComponent(String id) {
        DataTable dataTable;
        dataTable = (DataTable) JsfUtil.findComponent(id);
        if (dataTable != null) {
            dataTable.reset();
            dataTable.resetValue();
        }

    }
}
