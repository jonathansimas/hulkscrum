package com.simas.scrum.named;

import com.simas.scrum.dao.TarefaDAO;
import com.simas.scrum.entity.Equipe;
import com.simas.scrum.entity.Projeto;
import com.simas.scrum.entity.Sprint;
import com.simas.scrum.entity.Tarefa;
import com.simas.scrum.entity.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

/**
 *
 * @author c3712548
 */
@Named("quadro")
@RequestScoped
public class QuadroNamed {
    
    private String ajaxResponse;
    private String coluna;
    private String termoBusca;
    private Integer idTarefa;
    @Inject
    private Projeto projeto;
    @Inject
    private Sprint sprint;
    @Inject
    private Equipe equipe;
    @Inject
    private Usuario usuario;
    @Inject
    private TarefaNamed tarefaNamed;
    @Inject
    private LoginNamed loginNamed;
    @Inject
    private Tarefa tarefa;
    @Inject
    private TarefaDAO tarefaDAO;
    
    public enum statusTarefa {
        
        PENDENTE,
        ANDAMENTO,
        FINALIZADA,
        IMPEDIDA
    }
    
    public Sprint getSprint() {
        return sprint;
    }
    
    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }
    
    public Equipe getEquipe() {
        return equipe;
    }
    
    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }
    
    public Usuario getUsuario() {
        return usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public TarefaNamed getTarefaNamed() {
        return tarefaNamed;
    }
    
    public void setTarefaNamed(TarefaNamed tarefaNamed) {
        this.tarefaNamed = tarefaNamed;
    }
    
    public Tarefa getTarefa() {
        return tarefa;
    }
    
    public void setTarefa(Tarefa tarefa) {
        this.tarefa = tarefa;
    }
    
    public TarefaDAO getTarefaDAO() {
        return tarefaDAO;
    }
    
    public void setTarefaDAO(TarefaDAO tarefaDAO) {
        this.tarefaDAO = tarefaDAO;
    }
    
    public Projeto getProjeto() {
        return projeto;
    }
    
    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }
    
    public String getColuna() {
        return coluna;
    }
    
    public void setColuna(String coluna) {
        this.coluna = coluna;
    }
    
    public String getTermoBusca() {
        return termoBusca;
    }
    
    public void setTermoBusca(String termoBusca) {
        this.termoBusca = termoBusca;
    }
    
    public Integer getIdTarefa() {
        return idTarefa;
    }
    
    public void setIdTarefa(Integer idTarefa) {
        this.idTarefa = idTarefa;
    }
    
    public String getAjaxResponse() {
        return ajaxResponse;
    }
    
    public void setAjaxResponse(String ajaxResponse) {
        this.ajaxResponse = ajaxResponse;
    }
    
    public LoginNamed getLoginNamed() {
        return loginNamed;
    }
    
    public void setLoginNamed(LoginNamed login) {
        this.loginNamed = login;
    }
    
    public String moverTarefa() {
        FacesMessage msg = null;
        Tarefa tarefa = tarefaDAO.getById(idTarefa);
        tarefa.setStatus(Integer.valueOf(coluna));
        if (coluna.equals("1") || coluna.equals("2")) {
            tarefa.setUsuarioIdUsuario(getLoginNamed().getUsuario());
        }
     
       try {
            tarefaDAO.save(tarefa);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Ok", "A tarefa #" + idTarefa.toString() + "  foi alterada");
            FacesContext.getCurrentInstance().addMessage("growl", msg);
            setAjaxResponse("ok");
            return "ok";
        } catch (Exception e) {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Houve um erro ao alterar a tarefa #" + idTarefa.toString() + ".", e.getMessage());
            FacesContext.getCurrentInstance().addMessage("growl", msg);
            setAjaxResponse("error");
            return "error";
        }
        
        
    }
    
    public List<Tarefa> listarTarefas(Integer tipo) {
        Tarefa tarefaExemplo = new Tarefa();
        tarefaExemplo.setStatus(tipo);
        Criteria resultados = tarefaDAO.getSession().createCriteria(Tarefa.class);
        resultados.createCriteria("projetoIdProjeto", "p");
        resultados.createCriteria("usuarioIdUsuario", "u", JoinType.LEFT_OUTER_JOIN);
        resultados.createCriteria("sprintIdSprint", "s", JoinType.LEFT_OUTER_JOIN);
        resultados.add(Restrictions.eq("status", tipo));
        
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            resultados.add(Restrictions.like("nome", '%' + getTermoBusca() + "%"));
        }
        if (getProjeto() != null && getProjeto().getIdProjeto() != null) {
            resultados.add(Restrictions.eq("p.idProjeto", getProjeto().getIdProjeto()));
        }
        if (getUsuario() != null && getUsuario().getIdUsuario() != null) {            
            resultados.add(Restrictions.eq("u.idUsuario", getUsuario().getIdUsuario()));
        }        
        if (getSprint() != null && getSprint().getIdSprint() != null) {            
            resultados.add(Restrictions.eq("s.idSprint", getSprint().getIdSprint()));
        }
        
        return resultados.list();
    }
    
    public void limparListarTarefas() {
        this.setTermoBusca("");
        this.setProjeto(null);
        this.setUsuario(null);
        this.setSprint(null);
        this.listarTarefas();
    }
    
    public void carregarListarTarefas() {
    }
    
    public List<Tarefa> listarTarefas() {
        Tarefa tarefaExemplo = new Tarefa();
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            tarefaExemplo.setNome(termoBusca);
        }
        return tarefaDAO.getBySimilarity(tarefaExemplo);
        
    }
    
    @PostConstruct
    public void init() {
        HttpSession session;
        tarefaDAO = TarefaDAO.getInstance();
        if (FacesContext.getCurrentInstance() != null) {
            session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        }
    }
}
