package com.simas.scrum.named;

import com.simas.scrum.business.util.JsfUtil;
import com.simas.scrum.business.util.MessagesController;
import com.simas.scrum.dao.ProjetoDAO;
import com.simas.scrum.entity.Projeto;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author c3712548
 */
@Named("projeto")
@RequestScoped
public class ProjetoNamed {

    @Inject
    private Projeto projeto;
    @Inject
    private ProjetoDAO projetoDAO;
    private List<Projeto> projetos;
    private String termoBusca;
    private LazyDataModel<Projeto> lazyModel;
    private int totalRows;
    private int pageSize;

    public LazyDataModel<Projeto> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Projeto> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public String getTermoBusca() {
        return termoBusca;
    }

    public void setTermoBusca(String termoBusca) {
        this.termoBusca = termoBusca;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public ProjetoDAO getProjetoDAO() {
        return projetoDAO;
    }

    public void setProjetoDAO(ProjetoDAO projetoDAO) {
        this.projetoDAO = projetoDAO;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }

    public void save() {
          try {
            projeto.setDataCriacao(new Date());
            projetoDAO.save(projeto);
            MessagesController.addInfo(null, "O projeto foi salvo com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }
    }

    public void update() {
        try {
            projeto.setDataAtualizacao(new Date());
            projetoDAO.update(projeto);
            MessagesController.addInfo(null, "O projeto foi salvo com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }

    }

    public void delete() {
        projetoDAO.delete(projeto);
    }

    public void delete(Projeto projeto) {
        projetoDAO.delete(projeto);
    }

    public List<Projeto> listarProjetos() {
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            Projeto projetoExemplo = new Projeto();
            projetoExemplo.setNome(termoBusca);

            return projetoDAO.getBySimilarity(projetoExemplo);
        }
        return projetoDAO.getAll();
    }

    public String carregarEdicao(Projeto projeto) throws IOException {
        this.projeto = projeto;
        return "Edit.xhtml";
    }

    public String carregarNovaProjeto() {
        this.projeto = new Projeto();
        return "Create.xhtml";
    }

    public String carregarListarProjetos() {
        this.listarProjetos();
        return "List.xhtml";
    }

    public String limparListarProjetos() {
        this.setTermoBusca("");
        this.listarProjetos();
        return "List.xhtml";
    }

    @PostConstruct
    public void init() {
       HttpSession session;
        projetoDAO = ProjetoDAO.getInstance();
        if(FacesContext.getCurrentInstance() != null)
        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }

    public void updateComponent(String id) {
        DataTable dataTable;
        dataTable = (DataTable) JsfUtil.findComponent(id);
        if (dataTable != null) {
            dataTable.reset();
            dataTable.resetValue();
        }

    }
}
