package com.simas.scrum.named;

import com.simas.scrum.business.util.JsfUtil;
import com.simas.scrum.business.util.MessagesController;
import com.simas.scrum.dao.SprintDAO;
import com.simas.scrum.dao.UsuarioDAO;
import com.simas.scrum.entity.Sprint;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author c3712548
 */
@Named("sprint")
@RequestScoped
public class SprintNamed {

    @Inject
    private Sprint sprint;
    @Inject
    private SprintDAO sprintDAO;
    private List<Sprint> sprints;
    private String termoBusca;
    private LazyDataModel<Sprint> lazyModel;
    private int totalRows;
    private int pageSize;

    public LazyDataModel<Sprint> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Sprint> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public String getTermoBusca() {
        return termoBusca;
    }

    public void setTermoBusca(String termoBusca) {
        this.termoBusca = termoBusca;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public SprintDAO getSprintDAO() {
        return sprintDAO;
    }

    public void setSprintDAO(SprintDAO sprintDAO) {
        this.sprintDAO = sprintDAO;
    }

    public List<Sprint> getSprints() {
        return sprints;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }

    public void save() {
        try {
            sprintDAO.save(sprint);
            MessagesController.addInfo(null, "A sprint foi salva com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }

    }

    public void saveOrUpdate() {
        try {
            sprintDAO.saveOrUpdate(sprint);
            MessagesController.addInfo(null, "A sprint foi salva com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }

    }

    public void delete() {
        sprintDAO.delete(sprint);
    }

    public void delete(Sprint sprint) {
        sprintDAO.delete(sprint);
         sprintDAO.getSession().flush();
    }

    public List<Sprint> listarSprints() {
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            Sprint sprintExemplo = new Sprint();
            sprintExemplo.setObs(termoBusca);

            return sprintDAO.getBySimilarity(sprintExemplo);
        }
        return sprintDAO.getAll();
    }

    public String carregarEdicao(Sprint sprint) throws IOException {
        this.sprint = sprint;
        return "Edit.xhtml";
    }

    public String carregarNovaSprint() {
        this.sprint = new Sprint();
        return "Create.xhtml";
    }

    public String carregarListarSprints() {
        this.listarSprints();
        return "List.xhtml";
    }

    public String limparListarSprints() {
        this.setTermoBusca("");
        this.listarSprints();
        return "List.xhtml";
    }

    @PostConstruct
    public void init() {
        HttpSession session;
        sprintDAO = SprintDAO.getInstance();
        if (FacesContext.getCurrentInstance() != null) {
            session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        }
    }

    public void updateComponent(String id) {
        DataTable dataTable;
        dataTable = (DataTable) JsfUtil.findComponent(id);
        if (dataTable != null) {
            dataTable.reset();
            dataTable.resetValue();
        }

    }
}
