/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simas.scrum.named;

import com.simas.scrum.business.util.EncryptUtil;
import com.simas.scrum.business.util.MessagesController;
import com.simas.scrum.dao.UsuarioDAO;
import com.simas.scrum.entity.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author c3712548
 */
@Named("login")
@SessionScoped
public class LoginNamed implements Serializable {

    @Inject
    private Usuario usuario;
    @Inject
    private UsuarioDAO usuarioDAO;

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String logar() {
        Usuario exemplo = new Usuario();
        exemplo.setLogin(getUsuario().getLogin());
        exemplo.setSenha(EncryptUtil.md5(getUsuario().getSenha()));
        List<Usuario> usuarios = usuarioDAO.getByExample(exemplo);
        if(usuarios.isEmpty())
        {
             MessagesController.addWarn("Atenção", "Login ou senha Incorretos");
             return null;
        }
        setUsuario(usuarios.get(0));
        return "/quadro/index.xhtml?faces-redirect=true";
    }
    
    
        public String deslogar() {
        Usuario exemplo = new Usuario();
        exemplo.setLogin(null);
        exemplo.setSenha(null);       
        setUsuario(exemplo);      
        return "/login/Login.xhtml?faces-redirect=true";
    }
    
    
    @PostConstruct
    public void init() {
        HttpSession session;
        usuarioDAO = UsuarioDAO.getInstance();
        if(FacesContext.getCurrentInstance() != null)
        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }
}
