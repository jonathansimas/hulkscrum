package com.simas.scrum.named;

import com.simas.scrum.business.util.EncryptUtil;
import com.simas.scrum.business.util.JsfUtil;
import com.simas.scrum.business.util.MessagesController;
import com.simas.scrum.dao.UsuarioDAO;
import com.simas.scrum.entity.Usuario;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author c3712548
 */
@Named("usuario")
@RequestScoped
public class UsuarioNamed {

    @Inject
    private Usuario usuario;
    @Inject
    private UsuarioDAO usuarioDAO;
    private List<Usuario> usuarios;
    private String termoBusca;
    private String novaSenha;    
    private LazyDataModel<Usuario> lazyModel;
    private int totalRows;
    private int pageSize;
    

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }


    public LazyDataModel<Usuario> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Usuario> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public String getTermoBusca() {
        return termoBusca;
    }

    public void setTermoBusca(String termoBusca) {
        this.termoBusca = termoBusca;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public void save() {
        usuarioDAO.save(usuario); 
   }

    public void saveOrUpdate() {
        try {
           
            Usuario usuarioExemplo = new Usuario();
            usuarioExemplo.setLogin(getUsuario().getLogin());
            List<Usuario> jaExiste = usuarioDAO.getByExample(usuarioExemplo);
            if(!jaExiste.isEmpty())
            {
                if(jaExiste.get(0).getIdUsuario().equals(getUsuario().getIdUsuario()))
                {
                    MessagesController.addError("Já existe um usuário com este login", "Por favor, altere o campo login");
                }
                
            }
            
            
            if(!getNovaSenha().equals(""))
            {
                getUsuario().setSenha(EncryptUtil.md5(getNovaSenha()));
            }
            usuarioDAO.saveOrUpdate(usuario);
            MessagesController.addInfo("", "O usuario foi salvo com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }

    }
    public void update() {
        try {
           //Validation
            Usuario usuarioExemplo = new Usuario();
            usuarioExemplo.setLogin(getUsuario().getLogin());
            List<Usuario> jaExiste = usuarioDAO.getByExample(usuarioExemplo);
            if(!jaExiste.isEmpty())
            {
                if(!jaExiste.get(0).getIdUsuario().equals(getUsuario().getIdUsuario()))
                {
                    MessagesController.addError("Já existe um usuário com este login.", "Por favor, altere o campo login.");
                    return;
                }
                
            }
            
            
             if(!getNovaSenha().equals(""))
            {
                getUsuario().setSenha(EncryptUtil.md5(getNovaSenha()));
            }
            usuarioDAO.clear();
            usuarioDAO.update(getUsuario());
            MessagesController.addInfo("", "O usuario foi salvo com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }
 

    }
    public void delete() {
        usuarioDAO.delete(usuario);
    }

    public void delete(Usuario usuario) {
        usuarioDAO.delete(usuario);
    }

    public List<Usuario> listarUsuarios() {
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            Usuario usuarioExemplo = new Usuario();
            usuarioExemplo.setNome(termoBusca);

            return usuarioDAO.getBySimilarity(usuarioExemplo);
        }
        return usuarioDAO.getAll();
    }

    public String carregarEdicao(Usuario usuario) throws IOException {
      
        this.usuario = usuario;
        return "/usuario/Edit.xhtml";
    }

    public String carregarNovaUsuario() {
        this.usuario = new Usuario();
        return "Create.xhtml";
    }

    public String carregarListarUsuarios() {
        this.listarUsuarios();
        return "List.xhtml";
    }

    public String limparListarUsuarios() {
        this.setTermoBusca("");
        this.listarUsuarios();
        return "List.xhtml";
    }

    @PostConstruct
    public void init() {
        HttpSession session;
        usuarioDAO = UsuarioDAO.getInstance();
        if (FacesContext.getCurrentInstance() != null) {
            session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        }
    }

    public void updateComponent(String id) {
        DataTable dataTable;
        dataTable = (DataTable) JsfUtil.findComponent(id);
        if (dataTable != null) {
            dataTable.reset();
            dataTable.resetValue();
        }

    }
}
