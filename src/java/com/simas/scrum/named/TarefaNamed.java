package com.simas.scrum.named;

import com.simas.scrum.business.util.JsfUtil;
import com.simas.scrum.business.util.MessagesController;
import com.simas.scrum.dao.ProjetoDAO;
import com.simas.scrum.dao.TarefaDAO;
import com.simas.scrum.entity.Projeto;
import com.simas.scrum.entity.Tarefa;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author c3712548
 */
@Named("tarefa")
@RequestScoped
public class TarefaNamed {

    @Inject
    private Tarefa tarefa;
    @Inject
    private Projeto projeto;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }
    @Inject
    private TarefaDAO tarefaDAO;
    @Inject
    private ProjetoDAO projetoDAO;
    private List<Tarefa> tarefas;
    private String termoBusca;
    private LazyDataModel<Tarefa> lazyModel;
    private int totalRows;
    private int pageSize;

    public LazyDataModel<Tarefa> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Tarefa> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public String getTermoBusca() {
        return termoBusca;
    }

    public void setTermoBusca(String termoBusca) {
        this.termoBusca = termoBusca;
    }

    public Tarefa getTarefa() {
        return tarefa;
    }

    public void setTarefa(Tarefa tarefa) {
        this.tarefa = tarefa;
    }

    public TarefaDAO getTarefaDAO() {
        return tarefaDAO;
    }

    public void setTarefaDAO(TarefaDAO tarefaDAO) {
        this.tarefaDAO = tarefaDAO;
    }

    public List<Tarefa> getTarefas() {
        return tarefas;
    }

    public void setTarefas(List<Tarefa> tarefas) {
        this.tarefas = tarefas;
    }

    public void save() {
        try {
            tarefa.setDataCriacao(new Date());
            tarefaDAO.save(tarefa);
            MessagesController.addInfo(null, "A tarefa foi salva com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }
    }

    public void update() {
        try {
            tarefa.setDataAtualizacao(new Date());
            tarefaDAO.update(tarefa);
            MessagesController.addInfo(null, "A tarefa foi salva com Sucesso.");
        } catch (Exception e) {
            MessagesController.addError("Houve um erro na aplicação:", e.getMessage());
        }

    }

    public void delete() {
        tarefaDAO.delete(tarefa);
    }

    public void delete(Tarefa tarefa) {
        tarefaDAO.delete(tarefa);
    }

    public List<Tarefa> listarTarefas() {
        Tarefa tarefaExemplo = new Tarefa();
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            tarefaExemplo.setNome(termoBusca);
        }
//        if (getProjeto() != null && getProjeto().getIdProjeto() != null) {
//            tarefaExemplo.setProjetoIdProjeto(projeto);
//        }

        return tarefaDAO.getBySimilarity(tarefaExemplo);

    }

    public List<Projeto> listarProjetos() {
        if (getTermoBusca() != null && !getTermoBusca().equals("")) {
            Projeto projetoExemplo = new Projeto();
            projetoExemplo.setNome(termoBusca);

            return projetoDAO.getBySimilarity(projetoExemplo);
        }
        return projetoDAO.getAll();
    }

    public String carregarEdicao(Tarefa tarefa) throws IOException {
        this.tarefa = tarefa;
        return "Edit.xhtml";
    }

    public String carregarNovaTarefa() {
        this.tarefa = new Tarefa();
        return "Create.xhtml";
    }

    public String carregarListarTarefas() {
        this.listarTarefas();
        return "List.xhtml";
    }

    public String limparListarTarefas() {
        this.setTermoBusca("");
        this.listarTarefas();
        return "List.xhtml";
    }

    @PostConstruct
    public void init() {
        HttpSession session;
        tarefaDAO = TarefaDAO.getInstance();
        if (FacesContext.getCurrentInstance() != null) {
            session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        }
    }

    public void updateComponent(String id) {
        DataTable dataTable;
        dataTable = (DataTable) JsfUtil.findComponent(id);
        if (dataTable != null) {
            dataTable.reset();
            dataTable.resetValue();
        }

    }

    public ProjetoDAO getProjetoDAO() {
        return projetoDAO;
    }

    public void setProjetoDAO(ProjetoDAO projetoDAO) {
        this.projetoDAO = projetoDAO;
    }
}
