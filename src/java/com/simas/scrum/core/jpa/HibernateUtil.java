package com.simas.scrum.core.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class HibernateUtil {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static final String PERSISTENCE_UNIT_NAME = "scrumPU";


	public static EntityManager getEntityManager() {
		if (emf == null){
			emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		if (em == null){		
			em = emf.createEntityManager();
		}
		return em;
	}
	
	public static void fechar(){
		emf.close();
	}
	
	
 
}
