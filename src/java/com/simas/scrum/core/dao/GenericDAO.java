package com.simas.scrum.core.dao;

import com.simas.scrum.core.jpa.HibernateUtil;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public abstract class GenericDAO<T> implements Serializable {

    private Class type;

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    @SuppressWarnings({"deprecation", "unchecked"})
    public List<T> getByTitle(String valor) {
        Criteria notas = getSession().createCriteria(type);
        notas.add(Restrictions.ilike("titulo", valor, MatchMode.ANYWHERE));
        return notas.list();
    }

    public List<T> getByExample(Object obj) {
        Criteria resultados = getSession().createCriteria(type);
        resultados.add(Example.create(obj).excludeZeroes());
        List<T> resposta;
        resposta = resultados.list();
        try {
            resposta = resultados.list();
        } catch (Exception e) {
            resposta = new ArrayList<T>();
        }
        return resposta;
    }

   public List<T> getBySimilarity(Object obj) {
        Criteria resultados = getSession().createCriteria(type);
        resultados.add(Example.create(obj).enableLike(MatchMode.ANYWHERE));
       
        return resultados.list();
    }

    public List<T> getBySimilarity(Object obj, int first, int pageSize, String sortField, boolean sortOrder) {
        Criteria resultados = getSession().createCriteria(type);
        resultados.add(Example.create(obj).enableLike(MatchMode.ANYWHERE));
        resultados.setFirstResult(first);
        resultados.setMaxResults(pageSize);
        if (sortOrder) {
            resultados.addOrder(Order.asc(sortField));
        } else {
            resultados.addOrder(Order.desc(sortField));
        }
        List<T> result = resultados.list();
        return result;
    }
    
        public Long getCountBySimilarity(Object obj) {
        Criteria resultados = getSession().createCriteria(type);
        resultados.add(Example.create(obj).enableLike(MatchMode.ANYWHERE));   
        return (Long) resultados.setProjection(Projections.rowCount()).uniqueResult();
    }

    public List<T> getAll() {
        Criteria resultados = getSession().createCriteria(type);
        return resultados.list();
    }

    public T getById(Integer value) {
        Criteria resultados = getSession().createCriteria(type);
        resultados.add(Restrictions.idEq(value));
        return (T) resultados.uniqueResult();
    }

    public T getById(Long value) {
        Criteria resultados = getSession().createCriteria(type);
        resultados.add(Restrictions.idEq(value));
        return (T) resultados.list().get(0);
    }

    public Session getSession() {
        EntityManager em = null;
        em = HibernateUtil.getEntityManager();
        return (Session) em.getDelegate();
    }

    public void clear() {       
        this.getSession().clear();
    }

    public String delete(T obj) {
        try {
            getSession().delete(obj);
            getSession().flush();
        } catch (Exception e) {
            this.clear();
            return e.getMessage();
        }
        return null;
    }

    public boolean save(T obj) {
        getSession().save(obj);
        getSession().flush();
        return true;
    }

    public boolean saveOrUpdate(T obj) {
        getSession().saveOrUpdate(obj);
        getSession().flush();
        return true;
    }
    
        public boolean update(T obj) {
        getSession().update(obj);
        getSession().flush();
        return true;
    }
        
        
  
        
        
}