package com.simas.scrum.business.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


public class MessagesController {    
  
    public static void addInfo(String title, String msg) {  
         FacesContext.getCurrentInstance().addMessage("inFormMessage",getFM(FacesMessage.SEVERITY_INFO,title, msg));  
    }  
  
    public static void addWarn(String title, String msg) {  
        FacesContext.getCurrentInstance().addMessage("inFormMessage",getFM(FacesMessage.SEVERITY_WARN,title, msg));  
    }  
  
    public static void addError(String title, String msg) {  
        FacesContext.getCurrentInstance().addMessage("inFormMessage", getFM(FacesMessage.SEVERITY_ERROR,title, msg));  
    }  
  
    public static void addFatal(String title, String msg) {  
        FacesContext.getCurrentInstance().addMessage("inFormMessage", getFM(FacesMessage.SEVERITY_FATAL,title, msg));  
    }  
     public static FacesMessage getFM(FacesMessage.Severity gravidade,String title, String msg)
     {
        FacesMessage fm = new FacesMessage(gravidade, title, msg);
        fm.setSummary(title);
        fm.setDetail(msg);  
        fm.setSeverity(gravidade);
        return fm;
     }
    
}
