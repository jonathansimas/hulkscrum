CREATE DATABASE  IF NOT EXISTS `scrumdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `scrumdb`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: scrumdb
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `projeto_usuario`
--

DROP TABLE IF EXISTS `projeto_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto_usuario` (
  `projeto_id_projeto` int(11) NOT NULL,
  `usuario_id_usuario` int(11) NOT NULL,
  KEY `fk_projeto_usuario_projeto1` (`projeto_id_projeto`),
  KEY `fk_projeto_usuario_usuario1` (`usuario_id_usuario`),
  CONSTRAINT `fk_projeto_usuario_projeto1` FOREIGN KEY (`projeto_id_projeto`) REFERENCES `projeto` (`id_projeto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_projeto_usuario_usuario1` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto_usuario`
--

LOCK TABLES `projeto_usuario` WRITE;
/*!40000 ALTER TABLE `projeto_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `projeto_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projeto`
--

DROP TABLE IF EXISTS `projeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto` (
  `id_projeto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(45) NOT NULL,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id_projeto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto`
--

LOCK TABLES `projeto` WRITE;
/*!40000 ALTER TABLE `projeto` DISABLE KEYS */;
INSERT INTO `projeto` (`id_projeto`, `nome`, `descricao`, `data_criacao`, `data_atualizacao`) VALUES (1,'TestePro','sdfa adfasd','2013-05-13 00:00:00','2013-05-08 18:19:49'),(2,'Projeto #3','Teste de projeto 3','2013-05-06 00:00:00','2013-05-14 16:22:43'),(3,'Projeto Monografia Unopar','a merda da monografia','2013-05-08 18:21:04',NULL);
/*!40000 ALTER TABLE `projeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `equipe_id_equipe` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `login` varchar(45) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_usuario_equipe1` (`equipe_id_equipe`),
  CONSTRAINT `fk_usuario_equipe1` FOREIGN KEY (`equipe_id_equipe`) REFERENCES `equipe` (`id_equipe`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id_usuario`, `equipe_id_equipe`, `nome`, `email`, `senha`, `login`, `status`) VALUES (1,7,'Jonathan Simas','jonathansimas@gmail.com','4297f44b13955235245b2497399d7a93','jonathan2',1),(2,12,'James Bond','jamesbond@gmail.com','4297f44b13955235245b2497399d7a93','jbond',1),(3,1,'gfbdf','dfgdfg','202cb962ac59075b964b07152d234b70','dfgdfg',1),(4,6,'Test','test@gmail.com','098f6bcd4621d373cade4e832627b4f6','test',1),(5,1,'Jonathan Simas','jonathansimas@gmail.com','4297f44b13955235245b2497399d7a93','jonathan',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarefa`
--

DROP TABLE IF EXISTS `tarefa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarefa` (
  `id_tarefa` int(11) NOT NULL AUTO_INCREMENT,
  `projeto_id_projeto` int(11) NOT NULL,
  `sprint_id_sprint` int(11) DEFAULT NULL,
  `usuario_id_usuario` int(11) DEFAULT NULL,
  `descricao` text NOT NULL,
  `dificuldade` int(11) DEFAULT NULL,
  `tempo` int(11) DEFAULT NULL,
  `inicio` datetime DEFAULT NULL,
  `fim` datetime DEFAULT NULL,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `nome` varchar(45) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id_tarefa`),
  KEY `fk_tarefa_projeto1` (`projeto_id_projeto`),
  KEY `fk_tarefa_sprint1` (`sprint_id_sprint`),
  KEY `fk_tarefa_usuario1` (`usuario_id_usuario`),
  CONSTRAINT `fk_tarefa_projeto1` FOREIGN KEY (`projeto_id_projeto`) REFERENCES `projeto` (`id_projeto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tarefa_sprint1` FOREIGN KEY (`sprint_id_sprint`) REFERENCES `sprint` (`id_sprint`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tarefa_usuario1` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarefa`
--

LOCK TABLES `tarefa` WRITE;
/*!40000 ALTER TABLE `tarefa` DISABLE KEYS */;
INSERT INTO `tarefa` (`id_tarefa`, `projeto_id_projeto`, `sprint_id_sprint`, `usuario_id_usuario`, `descricao`, `dificuldade`, `tempo`, `inicio`, `fim`, `data_criacao`, `data_atualizacao`, `nome`, `status`) VALUES (5,1,NULL,NULL,'Teste2',2,8,NULL,NULL,'2013-05-07 00:00:00','2013-05-08 17:32:47','Teste1',0),(7,2,NULL,1,'Teste de cadastro de tarefa numero 2',3,1,NULL,NULL,'2013-05-07 00:00:00','2013-05-07 16:28:16','Tarefa 2',0),(8,1,NULL,NULL,'Teste de cadastro de tarefa numero 2',3,1,NULL,NULL,'2013-05-07 00:00:00','2013-05-16 17:51:54','Tarefa 3',0),(9,3,NULL,1,'No ADM tirar da capa o \"Ultimas Vendas\" e \"Ultimos Clientes Cadastrados\"',1,2,NULL,NULL,'2013-05-10 15:58:24',NULL,'Ultimos Clientes Cadastrados',1),(10,3,NULL,NULL,'Incluir Status Anuncios Pausados, Status 2	',2,3,NULL,NULL,'2013-05-10 15:59:00',NULL,'Anuncios Pausados, Status',1),(11,3,NULL,5,'Menu topo: fontSize: 16px, color:#777, negrito',2,3,NULL,NULL,'2013-05-10 15:59:25',NULL,'Menu topo',1),(12,3,NULL,5,'Autocomplete=off no campo de busca',2,3,NULL,NULL,'2013-05-10 15:59:45',NULL,'Autocomplete=off',2),(13,3,NULL,5,'Limitar categorias na busca ',2,3,NULL,NULL,'2013-05-10 15:59:54',NULL,'Limitar categorias na busca ',2),(14,3,NULL,5,'Banner topoFull: expandir sobre o site (criar funcao javascript oculta/mostra)',2,3,NULL,NULL,'2013-05-10 16:00:13',NULL,'Banner topoFull',2),(15,2,NULL,1,'Teste 3',2,3,NULL,NULL,'2013-05-16 16:37:01',NULL,'Teste',1);
/*!40000 ALTER TABLE `tarefa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissao`
--

DROP TABLE IF EXISTS `permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissao` (
  `id_permissao` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(145) NOT NULL,
  `descricao` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`id_permissao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissao`
--

LOCK TABLES `permissao` WRITE;
/*!40000 ALTER TABLE `permissao` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(145) DEFAULT NULL,
  `descricao` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permissao`
--

DROP TABLE IF EXISTS `role_permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permissao` (
  `role_id_role` int(11) NOT NULL,
  `permissao_id_permissao` int(11) NOT NULL,
  KEY `fk_role_permissao_role1` (`role_id_role`),
  KEY `fk_role_permissao_permissao1` (`permissao_id_permissao`),
  CONSTRAINT `fk_role_permissao_permissao1` FOREIGN KEY (`permissao_id_permissao`) REFERENCES `permissao` (`id_permissao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_permissao_role1` FOREIGN KEY (`role_id_role`) REFERENCES `role` (`id_role`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permissao`
--

LOCK TABLES `role_permissao` WRITE;
/*!40000 ALTER TABLE `role_permissao` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipe` (
  `id_equipe` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_equipe`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe`
--

LOCK TABLES `equipe` WRITE;
/*!40000 ALTER TABLE `equipe` DISABLE KEYS */;
INSERT INTO `equipe` (`id_equipe`, `nome`, `status`) VALUES (1,'A-Team',1),(2,'MasterEquipeZZZ',3),(6,'Masters of Universe!!',3),(7,'teste',9),(9,'Fefsss',6),(10,'Test6',0),(11,'Test8',7),(12,'frwe',9);
/*!40000 ALTER TABLE `equipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sprint`
--

DROP TABLE IF EXISTS `sprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sprint` (
  `id_sprint` int(11) NOT NULL AUTO_INCREMENT,
  `inicio` datetime DEFAULT NULL,
  `fim` datetime DEFAULT NULL,
  `obs` varchar(245) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id_sprint`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sprint`
--

LOCK TABLES `sprint` WRITE;
/*!40000 ALTER TABLE `sprint` DISABLE KEYS */;
INSERT INTO `sprint` (`id_sprint`, `inicio`, `fim`, `obs`, `nome`) VALUES (1,'2013-05-29 00:00:00',NULL,'Mai/Jun 2013','Sprint 1'),(2,'2013-05-21 00:00:00','2013-05-31 00:00:00','Sprint 2 obss','Sprint 2'),(3,'2013-05-02 00:00:00','2013-05-09 00:00:00','Sprint 3','');
/*!40000 ALTER TABLE `sprint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_role`
--

DROP TABLE IF EXISTS `usuario_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_role` (
  `usuario_id_usuario` int(11) NOT NULL,
  `role_id_role` int(11) NOT NULL,
  KEY `fk_usuario_role_usuario1` (`usuario_id_usuario`),
  KEY `fk_usuario_role_role1` (`role_id_role`),
  CONSTRAINT `fk_usuario_role_role1` FOREIGN KEY (`role_id_role`) REFERENCES `role` (`id_role`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_role_usuario1` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_role`
--

LOCK TABLES `usuario_role` WRITE;
/*!40000 ALTER TABLE `usuario_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-05-16 18:02:20
